from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import exists

from .models import ProxyModel, db_connect, create_all


class ProxyPipeline(object):
    """Proxy pipeline for storing scraped items in the database"""

    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Create tables.
        """
        engine = db_connect()
        create_all(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """
        Save items in the database.
        """
        session = self.Session()
        proxy_model = ProxyModel(**item)

        # check if this ip and port is already in db
        if not session.query(exists().where(
            ProxyModel.ip == item['ip'] and ProxyModel.port == item['port']
        )).scalar():
            try:
                session.add(proxy_model)
                session.commit()
            except:
                session.rollback()
                raise
            finally:
                session.close()

        return item
