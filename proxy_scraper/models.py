from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

from .settings import SQLALCHEMY_DATABASE_URI


Base = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(SQLALCHEMY_DATABASE_URI)


def create_all(engine):
    """
    Create all tables if its doesn`t exists
    """
    Base.metadata.create_all(engine)


class ProxyModel(Base):
    """Sqlalchemy proxy model"""
    __tablename__ = "proxy"
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    ip = Column(String, nullable=False)
    port = Column(String, nullable=False)
