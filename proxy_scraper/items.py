# -*- coding: utf-8 -*-

from scrapy import Item, Field


class ProxyItem(Item):
    """
    Base proxy item
    """
    ip = Field()
    port = Field()
